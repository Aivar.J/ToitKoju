﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToitKoju.Models
{
    public class Ingredient
    {
        public int IngredientID { get; set; }
        public string IngredientName { get; set; }

        public virtual ICollection<Detail> Recipes { get; set; }

        public Ingredient()
        {
            Recipes = new HashSet<Detail>();
        }
    }
}