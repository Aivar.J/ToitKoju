﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToitKoju.Models
{
    public class OrderDetail
    {
        public int OrderDetailId { get; set; }
        public int OrderNo { get; set; }
        public int RecipeID { get; set; }
        public int UnitsAmount { get; set; }
        public decimal Unit { get; set; }
        public virtual Recipy Recipy { get; set; }
        public virtual Order Order { get; set; }
    }
}