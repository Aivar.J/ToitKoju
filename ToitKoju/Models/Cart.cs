﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ToitKoju.Models
{
    public class Cart
    {

        public int RecipeID { get; set; }
        public int CartID { get; set; }
        public int UserID { get; set; }
        public int UnitsAmount { get; set; }
        public int Unit { get; set; }
        public string RecipeName { get; set; }
        public System.DateTime DateCreated { get; set; }
        public virtual Recipy Recipy { get; set; }
    }
}