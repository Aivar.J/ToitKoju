﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToitKoju.Models
{
    public partial class ShoppingCart
    {
        ToitKojuEntities shopDB = new ToitKojuEntities();
        string ShoppingCartID { get; set; }
        public const string CartSessionKey = "CartID";

        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartID = cart.GetCartId(context);
            return cart;
        }
        // Helper method to simplify shopping cart calls
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }
        //public void AddToCart(Recipy recipy)
        //{
        //    // Get the matching cart and album instances
        //    var cartItem = shopDB.Carts.SingleOrDefault(
        //        c => c.CartID == ShoppingCartID 
        //        && c.RecipeID == recipy.RecipeID);

        //    if (cartItem == null)
        //    {
        //        // Create a new cart item if no cart item exists
        //        cartItem = new Cart
        //        {
        //            RecipeID = recipy.RecipeID,
        //            CartID = ShoppingCartID,
        //            Count = 1,
        //            DateCreated = DateTime.Now
        //        };
        //        shopDB.Carts.Add(cartItem);
        //    }
        //    else
        //    {
        //        // If the item does exist in the cart, 
        //        // then add one to the quantity
        //        cartItem.Count++;
        //    }
        //    // Save changes
        //    shopDB.SaveChanges();
        //}
        //public int RemoveFromCart(int id)
        //{
        //    // Get the cart
        //    var cartItem = shopDB.Carts.Single(
        //        cart => cart.CartID == ShoppingCartID
        //        && cart.RecipeID == id);

        //    int itemCount = 0;

        //    if (cartItem != null)
        //    {
        //        if (cartItem.Count > 1)
        //        {
        //            cartItem.Count--;
        //            itemCount = cartItem.Count;
        //        }
        //        else
        //        {
        //            shopDB.Carts.Remove(cartItem);
        //        }
        //        // Save changes
        //        shopDB.SaveChanges();
        //    }
        //    return itemCount;
        //}
        //public void EmptyCart()
        //{
        //    var cartItems = shopDB.Carts.Where(
        //        cart => cart.CartID == ShoppingCartId);

        //    foreach (var cartItem in cartItems)
        //    {
        //        shopDB.Carts.Remove(cartItem);
        //    }
        //    // Save changes
        //    shopDB.SaveChanges();
        //}
        //public List<Cart> GetCartItems()
        //{
        //    return shopDB.Carts.Where(
        //        cart => cart.CartID == ShoppingCartId).ToList();
        //}
        //public int GetCount()
        //{
        //    // Get the count of each item in the cart and sum them up
        //    int? count = (from cartItems in shopDB.Carts
        //                  where cartItems.CartID == ShoppingCartId
        //                  select (int?)cartItems.Count).Sum();
        //    // Return 0 if all entries are null
        //    return count ?? 0;
        //}
        //public decimal GetTotal()
        //{
        //    // Multiply album price by count of that album to get 
        //    // the current price for each of those albums in the cart
        //    // sum all album price totals to get the cart total
        //    decimal? total = (from cartItems in shopDB.Carts
        //                      where cartItems.CartID == ShoppingCartId
        //                      select (int?)cartItems.Count *
        //                      cartItems.Album.Price).Sum();

        //    return total ?? decimal.Zero;
        //}
        //public int CreateOrder(Order order)
        //{
        //    decimal orderTotal = 0;

        //    var cartItems = GetCartItems();
        //    // Iterate over the items in the cart, 
        //    // adding the order details for each
        //    foreach (var item in cartItems)
        //    {
        //        var orderDetail = new OrderDetail
        //        {
        //            RecipeID = item.RecipeID,
        //            OrderNo = order.OrderNo,
        //            UnitsAmount = item.UnitsAmount,
        //            Unit = item.Unit
        //        };
        //        // Set the order total of the shopping cart
        //        orderTotal += (item.UnitsAmount*item.Unit);

        //        shopDB.OrderDetails.Add(orderDetail);

        //    }
        //    // Set the order's total to the orderTotal count
        //    order.Total = orderTotal;

        //    // Save the order
        //    shopDB.SaveChanges();
        //    // Empty the shopping cart
        //    EmptyCart();
        //    // Return the OrderId as the confirmation number
        //    return order.OrderNo;
        //}

        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] =
                        context.User.Identity.Name;
                }
                else
                {
                    // Generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();
                    // Send tempCartId back to client as a cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[CartSessionKey].ToString();

        }
        //When a user has logged in, migrate their shopping cart to
        //be associated with their username
        //public void MigrateCart(string userName)
        //{
        //    var shoppingCart = shopDB.Carts.Where(
        //        c => c.CartID == ShoppingCartID);

        //    foreach (Cart item in shoppingCart)
        //    {
        //        item.CartID = UserName;
        //    }
        //    shopDB.SaveChanges();
        //}
    }
}