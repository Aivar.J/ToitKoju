﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ToitKoju.Models
{
    public class Detail
    {
        public int RecipeID { get; set; }
        public string IngredientName { get; set; }


        public virtual ICollection<Ingredient> Ingredients { get; set; }

        public Detail()
        {
            Ingredients = new HashSet<Ingredient>();
        }
    }

}