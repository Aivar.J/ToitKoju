//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ToitKoju
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int OrderID { get; set; }
        public System.DateTime OrderDate { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public int UserID { get; set; }
        public int ReceipeID { get; set; }
        public Nullable<int> ActualEaters { get; set; }
        public string DeliveryInstructions { get; set; }
    
        public virtual Recipy Recipy { get; set; }
        public virtual User User { get; set; }
    }
}
