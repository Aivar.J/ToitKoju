﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToitKoju.ViewModels
{
    public class ShoppingCartViewModel
    {
        public List<Order> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}