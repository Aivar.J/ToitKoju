﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToitKoju;


namespace ToitKoju
{
    public partial class Detail
    {
        public string IngredientName { get; set; }
    }
}

namespace ToitKoju.Controllers
{
    public class DetailsController : Controller
    {
        private ToitKojuEntities db = new ToitKojuEntities();

        // GET: Details
        public ActionResult Index()
        {
            var details = db.Details.Include(d => d.Ingredient).Include(d => d.Recipy);
            var unit = db.Ingredients.Select(x => x.Unit);
            ViewBag.CurrentUnit = unit;
            
            return View(details.ToList());
        }

        // GET: Details/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detail detail = db.Details.Find(id);
            if (detail == null)
            {
                return HttpNotFound();
            }
           

            return View(detail);
        }

        // GET: Details/Create
        public ActionResult Create()
        {
            ViewBag.IngredientID = new SelectList(db.Ingredients, "IngredientID", "IngredientName");
            ViewBag.RecipeID = new SelectList(db.Recipies, "RecipeID", "RecipeName");
            return View();
        }

        // POST: Details/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DetailID,RecipeID,OrderNo,IngredientID,Comment,Unit,UnitsAmount")] Detail detail)
        {
            if (ModelState.IsValid)
            {
                db.Details.Add(detail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //else if (ModelState.IsValid)
            //{
            //    db.Details.Add(detail);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            ViewBag.IngredientID = new SelectList(db.Ingredients, "IngredientID", "IngredientName", detail.IngredientID);
            ViewBag.RecipeID = new SelectList(db.Recipies, "RecipeID", "RecipeName", detail.RecipeID);
            return View(detail);
        }

        // GET: Details/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detail detail = db.Details.Find(id);
            if (detail == null)
            {
                return HttpNotFound();
            }
            ViewBag.IngredientID = new SelectList(db.Ingredients, "IngredientID", "IngredientName", detail.IngredientID);
            ViewBag.RecipeID = new SelectList(db.Recipies, "RecipeID", "RecipeName", detail.RecipeID);
            return View(detail);
        }

        // POST: Details/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DetailID,RecipeID,OrderNo,IngredientID,Comment,Unit,UnitsAmount")] Detail detail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IngredientID = new SelectList(db.Ingredients, "IngredientID", "IngredientName", detail.IngredientID);
            ViewBag.RecipeID = new SelectList(db.Recipies, "RecipeID", "RecipeName", detail.RecipeID);
            return View(detail);
        }

        // GET: Details/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detail detail = db.Details.Find(id);
            if (detail == null)
            {
                return HttpNotFound();
            }
            return View(detail);
        }

        // POST: Details/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Detail detail = db.Details.Find(id);
            db.Details.Remove(detail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     

    }
}
