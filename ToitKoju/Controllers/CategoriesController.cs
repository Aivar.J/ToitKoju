﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToitKoju;
using ToitKoju.Models;
using System.IO;

namespace ToitKoju.Controllers
{
    public class CategoriesController : Controller
    {
        private ToitKojuEntities db = new ToitKojuEntities();

        //kategooria pildi lisamine
        public ActionResult CategoryImage(int? id) => CategorieImage(id);
        public ActionResult CategorieImage(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Category c = db.Categories.Find(id.Value);
            if (c == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (c.Picture?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = c.Picture.ToArray(); //tüüp on pildil byte array, loetakse terve pilt
            if (picture[0] == 21) picture = picture.Skip(78).ToArray(); //selle võib ära unustada
            return File(picture, "image/jpg");
        }

        // GET: Categories
        public ActionResult Index()
        {
            var category = db.Categories.ToList();
            return View(category);
        }

        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            //TODO: tegime retseptide viewbagi
            ViewBag.Retseptid = new SelectList(db.Recipies.Where(x => x.CategoryID == category.CategoryID).ToList(), "RecipeID", "RecipeName");
            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryID,CategoryName,Description,Picture")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryID,CategoryName,Description,Picture")] Category category, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.Entry(category).Property(x => x.Picture).IsModified = false; //kas fail on olemas?
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //teeb streamreaderi, vormi input võetakse faili sisuks
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        category.Picture = buff; //võta pildi väärtuseks puhver
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.Categories.Find(id);
            db.Categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
