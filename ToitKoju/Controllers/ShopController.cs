﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToitKoju.Models;

namespace ToitKoju.Controllers
{
    public class ShopController : Controller

    {



        private ToitKojuEntities shopDB = new ToitKojuEntities();
        //kategooria pildi lisamine
        public ActionResult CategoryImage(int? id) => CategorieImage(id);
        public ActionResult CategorieImage(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Category c = shopDB.Categories.Find(id.Value);
            if (c == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (c.Picture?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = c.Picture.ToArray(); //tüüp on pildil byte array, loetakse terve pilt
            if (picture[0] == 21) picture = picture.Skip(78).ToArray(); //selle võib ära unustada
            return File(picture, "image/jpg");
        }


        //
        // GET: kategooriate nimekiri
        public ActionResult Index()
        {
            var category = shopDB.Categories.ToList();
            return View(category);
        }

        //
        // GET: /Shop/Browse
        public ActionResult Browse(string category)
        {
            var ostukorv = (Dictionary<int, Cart>)System.Web.HttpContext.Current.Session["ostukorv"];
            ViewBag.Ostukorv = ostukorv;
            //Kuvab kategooria seotuse retseptiga
            //var categoryModel = shopDB.Categories.Include("Recipies")
            //    .Single(g=>g.CategoryName==category);
            var c = shopDB.Categories.Where(x => x.CategoryName == category).SingleOrDefault();
            if (c == null) return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            return View(c);
        }

        //
        // GET: /Shop/Details/5
        public ActionResult Details(int id)
        {
            var recipy = shopDB.Recipies.Find(id);
            return View(recipy);
        }
        // GET: Shop/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = shopDB.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Shop/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryID,CategoryName,Description,Picture")] Category category, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                shopDB.Entry(category).State = EntityState.Modified;
                shopDB.Entry(category).Property(x => x.Picture).IsModified = false; //kas fail on olemas?
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //teeb streamreaderi, vormi input võetakse faili sisuks
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        category.Picture = buff; //võta pildi väärtuseks puhver
                    }
                }

                shopDB.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public ActionResult AddCart(int? id)
        {
            var ostukorv = (Dictionary<int, Cart>)System.Web.HttpContext.Current.Session["ostukorv"];
            var retsept = shopDB.Recipies.Find(id.Value);
            Cart c = ostukorv.Where(x => x.Value.RecipeID == id.Value).Select(x => x.Value).SingleOrDefault();
            if (c == null)
            {
                int nr = ostukorv.Keys.DefaultIfEmpty().Max() + 1;
                ostukorv[nr] = new Cart { RecipeID = id.Value, UnitsAmount = 1, RecipeName =  retsept.RecipeName};
            }
            else c.UnitsAmount++;
            ViewBag.Ostukorv = ostukorv;
            //var Retseptid = shopDB.Recipies.ToDictionary(x => x.RecipeID, x => x);
            //ViewBag.Ostukorv2 = ostukorv.Select(x => new { x.Key, x.Value.RecipeID, x.Value.UnitsAmount, ReceiptName = Retseptid[x.Value.RecipeID].RecipeName });
            return View("Browse", shopDB.Recipies.Find(id.Value).Category); // new { category = shopDB.Recipies.Find(id.Value).Category.CategorieName });
        }

        public ActionResult RemoveCart(int? id)
        {
            var ostukorv = (Dictionary<int, Cart>)System.Web.HttpContext.Current.Session["ostukorv"];
            Cart c = ostukorv[id.Value];
            Category t = shopDB.Recipies.Find(c.RecipeID).Category;
            if (c.UnitsAmount <= 1) ostukorv.Remove(id.Value);
            else c.UnitsAmount--;
            ViewBag.Ostukorv = ostukorv;
            return View("Browse", t); // new { category = shopDB.Recipies.Find(id.Value).Category.CategorieName });
        }


        public ActionResult ConfirmCart()
        {
            var ostukorv = (Dictionary<int, Cart>)System.Web.HttpContext.Current.Session["ostukorv"];
            foreach(var cart in ostukorv.Values)
            {
                shopDB.Orders.Add(new Order { OrderDate = DateTime.Now, DeliveryDate = DateTime.Now.AddDays(2), ActualEaters = cart.UnitsAmount, ReceipeID = cart.RecipeID, UserID = ToitKoju.User.GetByEmail(User.Identity.Name).UserID });
                shopDB.SaveChanges();
            }

            return RedirectToAction("Index", "Orders");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Tellimus([Bind(Include = "OrderID,OrderDate,DeliveryDate,UserID,ReceipeID,ActualEaters,DeliveryInstructions")] Order order)
        //{
        //    var ostukorv = (Dictionary<int, Cart>)System.Web.HttpContext.Current.Session["ostukorv"];
        //    var orders = shopDB.Orders.Include(o => o.Recipy).Include(o => o.User);
        //    foreach (KeyValuePair<int, Cart> x in ViewBag.Ostukorv)
        //    {
        //        DateTime OrderDate = new DateTime(05 / 06 / 2018);
        //        DateTime DeliveryDate = new DateTime(05 / 06 / 2018);
        //        int UserID = x.Value.UserID;
        //        int RecipeID = x.Value.RecipeID;
        //        int ActualEaters = x.Value.UnitsAmount;

        //        if (ModelState.IsValid)
        //        {
        //            shopDB.Orders.Add(order);
        //            shopDB.SaveChanges();
        //        }

        //    }
        //    return View("Orders");
        //}

    }
}