﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToitKoju
{
    public partial class Category
    {
        public string CategorieName { get; set; }
    }

}

namespace ToitKoju.Controllers
{
    public class HomeController : Controller

    {
        private ToitKojuEntities db = new ToitKojuEntities();
        public string CategoryName(int? id) 
        {
            if (id == null) return "";
            Category c = db.Categories.Find(id.Value);
            if (c == null) return "";
            return c.CategoryName;

        }

        //get home
        public ActionResult Index()
        {
            var category = db.Categories.ToList();
            return View(category.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Blog()
        {
            ViewBag.Message = "Home 1 vaade.";

            return View();
        }

    }
}