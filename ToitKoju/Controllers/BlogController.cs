﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ToitKoju.Controllers
{
    public class BlogController : Controller
    {
        // GET: Home1
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Blog()
        {
            ViewBag.Message = "Blogi vaade.";

            return View();
        }
    }
}