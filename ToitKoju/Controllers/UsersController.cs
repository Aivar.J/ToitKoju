﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToitKoju;
using T = ToitKoju;

namespace ToitKoju
{
    partial class User
    {
        static ToitKojuEntities db = new ToitKojuEntities();

        public static User GetByEmail(string email)
          => db.Users.Where(x => x.EMail.ToLower() == email.ToLower()).SingleOrDefault();

        //TODO kas kasutajal on mingi roll bool
        public bool IsInRoleAdmin => this.UserRole.Trim().ToLower() == "admin";

        public bool IsInRole(string role) => UserRole.Trim().ToLower() == role.ToLower();

    }

}

namespace ToitKoju.Controllers
{
    public class UsersController : Controller
    {
        private ToitKojuEntities db = new ToitKojuEntities();

        // GET: Users
        //public ActionResult Index()
        //{
        //    if (!User.Identity.IsAuthenticated)
        //        return RedirectToAction("Index", "Home");

        //    //kas on meie kasutaja
        //    User u = T.User.GetByEmail(User.Identity.Name);
        //    if (u== null) return RedirectToAction("Index", "Home");

        //    ViewBag.CurrentUser = u;

        //    return View(db.Users.ToList());
        //}

        //GET: Users
        public ActionResult Index()
        {
            User u = T.User.GetByEmail(User.Identity.Name);
            if (u == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            else
                if (u.IsInRoleAdmin)
            {
                var users = db.Users;
                return View(users.ToList());
            }
            else
            {
                var users = db.Users.Where(x => x.UserID == u.UserID);
                return View(users.ToList());
            }
        }

        // GET: Users/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            //kas on sisse loginud
  

            if (user == null) return RedirectToAction("Index", "Home");


            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,UserName,UserRole,EMail,Phone,Address,City")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,UserName,UserRole,EMail,Phone,Address,City")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
