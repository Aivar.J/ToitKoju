﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToitKoju;
using System.IO;
using Newtonsoft.Json;
using ToitKoju.Models;

namespace ToitKoju
{
    public partial class Recipy
    {
        public string IngredientName { get; set; }
        public int IngredientID { get; set; }
        public virtual Ingredient Ingredient { get; set; }
    }
}


namespace ToitKoju.Controllers
{
    public class RecipiesController : Controller
    {
        private ToitKojuEntities db = new ToitKojuEntities();
       
        //Retsepti pildi lisamise osa, meetod on retsepti image
        public ActionResult RecipyImage(int? id) => RecipeImage(id);
        public ActionResult RecipeImage(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Recipy r = db.Recipies.Find(id.Value);
            if (r == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (r.Picture?.Length == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var picture = r.Picture.ToArray(); //tüüp on pildil byte array, loetakse terve pilt
            if (picture[0] == 21) picture = picture.Skip(78).ToArray(); //selle võib ära unustada
            return File(picture, "image/jpg");
        }

        public string IngredientName(int? id)
        {
            if (id == null) return "";
            Ingredient i = db.Ingredients.Find(id.Value);
            if (i == null) return "";
            return i.IngredientName;
        }

        public string RecipeName(int? id) //éi saa aru mis see teeb
        {
            if (id == null) return "";
            Recipy r = db.Recipies.Find(id.Value);
            if (r == null) return "";
            return r.RecipeName;

        }

        public string Recipy(int? id)
        {
            if (id == null) return null;
            Recipy r = db.Recipies.Find(id.Value);
            if (r == null) return null;
            string s = JsonConvert.SerializeObject(new { r.RecipeID, r.RecipeName, r.Instructions });
            return s;
        }

        // GET: Recipies
        public ActionResult Index()
        {
            var recipies = db.Recipies.Include(r => r.Category).Include(r => r.User)
                .Include(r => r.Details)
                ;
            return View(recipies.ToList());
        }

        //Proovin lisada koostisosade nimekirja
        // GET: Recipies/Ingredient
        //koostisosade nimekiri
        public ActionResult Ingredient(int id)
        {
            //var ingredient = db.Ingredients.ToList();
            //return View(ingredient);
            var recipy = db.Recipies.Find(id);
            return View(recipy);
        }
        // GET: Recipies/Ingredient/List
        //koostisosade nimekiri retsepti lõikes
        public ActionResult List(string ingredient)
        {
            //Kuvab kategooria seotuse retseptiga
            //var categoryModel = shopDB.Categories.Include("Recipies")
            //    .Single(g=>g.CategoryName==category);
            var c = db.Ingredients.Where(x => x.IngredientName == ingredient).SingleOrDefault();
            //if (c == null) return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable);
            return View(c);
        }

        // GET: Recipies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipy recipy = db.Recipies.Find(id);
            if (recipy == null)
            {
                return HttpNotFound();
            }
           
            ViewBag.Koostisosad = new SelectList(db.Details.Where(x => x.RecipeID == recipy.RecipeID).ToList(), "IngredientID", "IngredientID");
            ViewBag.Nimetused = new SelectList(db.Ingredients, "IngredientName", "IngredientID"); //seda hetkel ei kasuta
            ViewBag.Kogus = new SelectList(db.Details.Where(x => x.RecipeID == recipy.RecipeID).ToList(), "UnitsAmount", "IngredientID");
            ViewBag.Yhik = new SelectList(db.Details.Where(x => x.RecipeID == recipy.RecipeID).ToList(), "Unit", "IngredientID");
            return View(recipy);
        }

        // GET: Recipies/Create
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName");
            ViewBag.Author = new SelectList(db.Users, "UserID", "UserName");
            //ViewBag.DetailID = new SelectList(db.Details, "DetailID", "RecipeID");
            return View();
        }

        // POST: Recipies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RecipeID,RecipeName,CategoryID,Author,Picture,TargetAmount,Instructions")] Recipy recipy)
        {
            if (ModelState.IsValid)
            {
                db.Recipies.Add(recipy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", recipy.CategoryID);
            ViewBag.Author = new SelectList(db.Users, "UserID", "UserName", recipy.Author);
            //ViewBag.DetailID = new SelectList(db.Details, "Detail", "DetailID", recipy.RecipeName);
            return View(recipy);
        }

        // GET: Recipies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipy recipy = db.Recipies.Find(id);
            if (recipy == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", recipy.CategoryID);
            ViewBag.Author = new SelectList(db.Users, "UserID", "UserName", recipy.Author);
            ViewBag.Ingredient = new SelectList(db.Ingredients, "IngredientID", "IngredientName", recipy.IngredientName);
            return View(recipy);
        }

        // POST: Recipies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RecipeID,RecipeName,CategoryID,Author,Picture,TargetAmount,Instructions")] Recipy recipy, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recipy).State = EntityState.Modified; //vormilt sisestatud element, märgi palun et see on muutunud
                //siia vahele lisasin retsepti pildi osa
                //järgm lause ütleb et on tehtud muudatus ja tuleb apdeitida
                db.Entry(recipy).Property(x => x.Picture).IsModified = false; //kas fail on olemas?
                if (file != null && file.ContentLength > 0)
                {
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //teeb streamreaderi, vormi input võetakse faili sisuks
                    {
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        recipy.Picture = buff; //võta pildi väärtuseks puhver
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");//tagasi indeksisse
            }//proovisin seda ümber teha, ei olnud abi
            //ViewBag.RecipeID = new SelectList(db.Recipies, "RecipeID", "RecipeName", recipy.CategoryID);
            //ViewBag.Author = new SelectList(db.Users, "UserID", "UserName", recipy.Author);
            //return View(recipy);
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", recipy.CategoryID);
            ViewBag.Author = new SelectList(db.Users, "UserID", "UserName", recipy.Author);
            ViewBag.Ingredient = new SelectList(db.Ingredients, "IngredientID", "IngredientName", recipy.IngredientName);
            return View(recipy);
        }

        // GET: Recipies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipy recipy = db.Recipies.Find(id);
            if (recipy == null)
            {
                return HttpNotFound();
            }
            return View(recipy);
        }

        // POST: Recipies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipy recipy = db.Recipies.Find(id);
            db.Recipies.Remove(recipy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
