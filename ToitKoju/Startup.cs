﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ToitKoju.Startup))]
namespace ToitKoju
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
