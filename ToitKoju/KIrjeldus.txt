﻿siia teeme oma projekti kirjeduse
Rühma liikmed: Helena Oras, Aivar Jürjan, Marit Linke

Rühmatöö lühikirjeldus: Loome rakenduse, mille eesmärgiks on inimesele pakkuda toiduainete koju tellimise võimalust vastavalt retseptile ja sööjate kogusele, mille ta rakenduses määrab. Veebrirakendus annab inimesele valida erinevate retseptide vahel, kus on olemas toidu foto, vajalikud koostisosas ning valmistamise kirjeldus. Rakendus kontrollib vastavalt valitud retseptile/retseptidele andmebaasist toiduainete laoseisu. Vastavalt sööjate arvule, komplekteeritakse vajalik kogus toiduaineid. Tellija tasub arve ning toiduained tuuakse kulleriga valitud ajal inimesele koju. 

Funktsionaalsus:


  Kõik kasutajad saavad rakendusse sisse logida (kasutajanimi, parool)
  Rakenduse avalehel kuvame juhuvalikuga retseptide pildid, sisselogimise nupud


Klient:


  saab end kasutajaks registreerida või kasutada teenust külalisena
  saab valida meeldiva retsepti ja sisestada sööjate arvu, kellele ta plaanib toitu valmistada
  valitud retseptide koostisosade kogused ja maksumus kuvatakse kliendi ostukorvis
  retsepte kuvatakse kliendile juhuvaliku järgi, klient saab otsida märksõnadega, retsepti kategooria (nt hommikusöök, praad, magustoit jms)
  määrata toiduainete kohale toomise aja ja koha (aadress)
  kinnitab tellimuse ja saab rakendusest automaatteavituse e-mailile (sel hetkel võetakse kogused laoseisust maha)


Administraator: 


  Administraator saad sisestada retseptid ja neid muuta/kustutada, lisada märksõnu (tag-e)
  Saab vaadata toiduainete laoseisu hetkeseis (kogus) ja tooteid lisada/kustutada
  Saab vaadata esitatud tellimuste arvu, sisu tellimuste komplekteerimiseks